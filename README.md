# Pig Tournament Runner

This is a simple script to run a
[round-robin](https://en.wikipedia.org/wiki/Round-robin_tournament) tournament
of [Pig](https://en.wikipedia.org/wiki/Pig_(dice_game)), the folk dice game,
and report some statistics. Here's how to use it:

1. Add players to `tournament/players.py` by following the initial examples.

2. Run the tournament with, e.g., `python -m tournament`.

3. Check the generated files for results:
    - `total_wins.csv` contains how many game wins each player earned in total.
    - `match_results.csv` contains how many wins each player earned in each individual matchup.


The tournament parameters can be changed by editing the constants at the top of `__main__.py`.

Yes, the script is rough around the edges. This is a fun weekend project, not a PR for work. :^)
