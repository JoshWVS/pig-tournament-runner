from collections import Counter, defaultdict, namedtuple
from dataclasses import dataclass
from itertools import combinations, count, cycle, permutations
import random

WINNING_SCORE = 30
ROUNDS_PER_MATCH = 100
MAX_TURNS = 1000


@dataclass
class GameResult:
    winner: str
    scores: dict[str, int]
    n_rounds: int


def roll_d6():
    return random.randint(1, 6)


def play_pig_game(players) -> GameResult:
    """Play a full game of Pig"""
    n_rounds = 0
    scores = defaultdict(int)
    # Note: relying on dictionary order for turn order!
    turn_order = cycle(permutations(players.keys()))
    while n_rounds < MAX_TURNS:
        n_rounds += 1
        active_player, opponent = next(turn_order)
        strategy = players[active_player]
        turn_score = play_pig_turn(strategy, scores[active_player], scores[opponent])
        scores[active_player] += turn_score
        if scores[active_player] >= WINNING_SCORE:
            return GameResult(
                winner=active_player,
                scores=scores,
                n_rounds=n_rounds,
            )

    raise RuntimeError(f"Ending this game after the limit of {MAX_TURNS} rounds...")


def play_pig_turn(strategy, player_score, opponent_score):
    """Play one turn of Pig"""
    ...
    subtotal = 0
    roll = roll_d6()
    while (roll := roll_d6()) != 1:
        subtotal += roll
        # if the player has a winning score, force them to stop
        if player_score + subtotal >= WINNING_SCORE:
            return subtotal

        try:
            result = strategy(player_score, opponent_score, roll, subtotal)
            if result not in {"ROLL", "HALT"}:
                raise RuntimeError(f"Invalid return value: '{result}'")
        except Exception as e:
            print(f"You broke the rules. You lose. GOOD DAY, SIR!!\n{e}")

        if result == "HALT":
            return subtotal
    # The player busted!
    return 0


def play_tournament(players):
    total_wins = Counter()
    match_results = []
    pairings = combinations(PLAYERS.keys(), 2)

    def _make_round_dict(pairing):
        return {name: players[name] for name in pairing}

    for pairing in pairings:
        match_result = Counter()
        p1_first = _make_round_dict(pairing)
        for _ in range(ROUNDS_PER_MATCH // 2):
            game_result = play_pig_game(p1_first)
            total_wins[game_result.winner] += 1
            match_result[game_result.winner] += 1

        # Alternate who gets to be starting player
        p2_first = _make_round_dict(reversed(pairing))
        for _ in range(ROUNDS_PER_MATCH // 2):
            game_result = play_pig_game(p2_first)
            total_wins[game_result.winner] += 1
            match_result[game_result.winner] += 1

        match_summary = match_result.most_common()
        match_winner, match_wins = match_summary[0]
        match_loser, _ = match_summary[1]
        match_results.append((match_winner, match_loser, match_wins))
        match_results.append((match_loser, match_winner, ROUNDS_PER_MATCH - match_wins))

    return total_wins, match_results


if __name__ == "__main__":
    from .players import PLAYERS
    random.seed(20240615)
    total_wins, match_results = play_tournament(PLAYERS)
    overall_ranking = total_wins.most_common()
    indexed_ranking = {name: rank for (name, _), rank in zip(overall_ranking, count(1))}
    # Sort match results by first player's overall rank, descending
    match_results.sort(key=lambda row: indexed_ranking[row[0]])

    with open("total_wins.csv", "w") as f:
        f.write("player,wins\n")
        for player, wins in overall_ranking:
            f.write(f"{player},{wins}\n")

    with open("match_results.csv", "w") as f:
        f.write("player_1,player_2,player_1_wins\n")
        for p1, p2, p1_wins in match_results:
            f.write(f"{p1},{p2},{p1_wins}\n")
