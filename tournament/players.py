from functools import wraps

PLAYERS = {}

def add_contestant(player_name):
    def inner_decorator(f):
        if player_name in PLAYERS:
            raise ValueError(f"duplicate stategies for {player_name}")
        PLAYERS[player_name] = f
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)
        return wrapper

    return inner_decorator


# This strategy tries to keep rolling until it has a winning score. It usually
# busts before then, but when it wins, it wins big!
@add_contestant("josh")
def everything_or_nothing(my_score, opponent_score, current_roll, subtotal):
    if subtotal >= WINNING_SCORE:
        return "HALT"
    else:
        return "ROLL"

# This strategy is very conservative: it always stops rolling after the first
# roll.
@add_contestant("liv")
def shy_guy(my_score, opponent_score, current_roll, subtotal):
    return "HALT"
